import React, { Component } from 'react';
import uuid from 'uuid'
import PropTypes from 'prop-types';

class AddProject extends Component {
    constructor() {
        super();
        this.state = {
            newProject: {}
        }
    }
    static defaultProps = {
        categories: ['Web Design', 'Web Development', 'Mobile Development']
    };
    handleSubmit(e){
        e.preventDefault();

        let title = this.refs.title.value;
        console.log("title", title);
        if (title === '') {
            alert("Title is required");
            return;
        }
        this.setState({
            newProject: {
                title: title,
                category: this.refs.category.value,
                id: uuid.v4()
            }
        }, function () {
            console.log("setState callback state", this.state);
            this.props.addProject(this.state.newProject);
        });
        console.log("submitted");
    }
    render() {
        let categoryOptions = this.props.categories.map(category => {
            return <option key={category} value={category}>{category}</option>
        });
        return (
            <div>
                <h3>Add Project</h3>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <label>Title</label><br/>
                        <input type="text" ref="title"/>
                    </div>
                    <div>
                        <label>Category</label><br/>
                        <select ref="category">
                            {categoryOptions}
                        </select>
                    </div>
                    <br/>
                    <input type="submit" value="Submit"/>
                    <br/>
                </form>
            </div>
        );
    }
}

AddProject.propTypes = {
    categories: PropTypes.array,
    addProject: PropTypes.func
};

export default AddProject;
