import React, { Component } from 'react';
import uuid from 'uuid'
import Projects from './Components/Projects'
import AddProject from './Components/AddProject'
import $ from 'jquery'
import Todos from './Components/Todos';

class App extends Component {
  constructor() {
    super();
    this.state = {
      projects: [],
        todos: []
    }
  }

  getTodos(){
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/todos',
        dataType: 'json',
        cache: false,
        success: function (data) {
            this.setState({todos: data}, function () {
                console.log(this.state);
            });
        }.bind(this),
        error: function(xhr, status, err) {
            console.error(err);
        }
    });
  }

  getProjects(){
      this.setState({projects: [
          {title: 'Business Website', category: "Web design", id: uuid.v4()},
          {title: 'Social App', category: "Mobile development", id: uuid.v4()},
          {title: 'Ecomerce shopping cart', category: "Web development", id: uuid.v4()}
      ]});
  }

  componentWillMount(){
      this.getProjects();
      this.getTodos();
  }

    componentDidMount(){
        this.getTodos();
    }

  handleAddProject(project){
    console.log("handleAddProject", project);
    let projects = this.state.projects;
    projects.push(project);
    this.setState({projects: projects});
  }

  handleDeleteProject(id){
      let projects = this.state.projects;
      let index = projects.findIndex(x => x.id === id);
      projects.splice(index, 1);
      this.setState({projects: projects});
  }

  render() {
    return (
      <div className="App">
          <AddProject addProject={this.handleAddProject.bind(this)}/>
          <Projects test="Hello world" projects={this.state.projects} onDelete={this.handleDeleteProject.bind(this)}/>
          <hr/>
          <Todos todos={this.state.todos}/>
      </div>
    );
  }
}

export default App;
